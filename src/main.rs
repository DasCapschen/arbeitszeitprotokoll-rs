/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

extern crate async_std;
extern crate futures;
extern crate regex;
#[macro_use]
extern crate time;
#[macro_use]
extern crate zbus;

use std::sync::{Arc, RwLock};
use async_std::{channel, task};
use futures_util::future::join_all;
use gtk::prelude::ApplicationExtManual;
use time::OffsetDateTime;

use dbus::Dbus;

use crate::logfile_writer::LogFile;
use crate::worktime::{WorkTimeKind, WorkTimeStorage};

mod dbus;
mod logfile;

mod gui;
mod locked_watcher;
mod notification_handler;
mod worktime;

#[async_std::main]
async fn main() {
    let (sigint_tx, sigint_rx) = channel::bounded(1);
    let (unlock_tx, unlock_rx) = channel::unbounded::<OffsetDateTime>();
    let (lock_tx, lock_rx) = channel::unbounded::<OffsetDateTime>();
    let (notification_tx, notification_rx) = channel::unbounded::<(OffsetDateTime, WorkTimeKind)>();

    let locked_watcher_task = task::spawn(locked_watcher::run(unlock_tx, lock_tx, sigint_rx));
    let notification_task = task::spawn(notification_handler::run(unlock_rx, notification_tx));
    let writer_task = task::spawn(logfile::logfile_writer::run(lock_rx, notification_rx));

    let storage: WorkTimeStorage = Arc::new(RwLock::new(Vec::new()));

    let reader_task = task::spawn(logfile::logfile_reader::run(storage.clone()));
    gui::get_application(storage).run(); // wait for gtk application to stop

    sigint_tx.send(()).await.expect("failed sending sigint msg"); // message locked watcher to stop
    locked_watcher_task.await; // wait for locked watcher to shutdown (cannot cancel, needs to send `stop` to writer)

    // wait for all the other tasks at once
    join_all(vec![notification_task, writer_task, reader_task]).await;
}
