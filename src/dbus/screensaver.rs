/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

#[dbus_proxy(
    default_service = "org.freedesktop.ScreenSaver",
    interface = "org.freedesktop.ScreenSaver",
    default_path = "/org/freedesktop/ScreenSaver"
)]
pub trait ScreenSaver {
    fn get_active(&self) -> zbus::Result<bool>;

    #[dbus_proxy(signal)]
    fn active_changed(&self, active: bool) -> zbus::Result<()>;
}
