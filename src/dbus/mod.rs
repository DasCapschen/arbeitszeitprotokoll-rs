/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use zbus::zvariant;

mod logind;
pub mod notification;
mod screensaver;

pub struct Dbus {
    session: zbus::Connection,
    system: zbus::Connection,
}

impl Dbus {
    pub async fn new() -> Self {
        Dbus {
            session: zbus::Connection::session()
                .await
                .expect("could not connect to session bus"),
            system: zbus::Connection::system()
                .await
                .expect("could not connect to system bus"),
        }
    }
    #[allow(dead_code)]
    pub async fn service_screensaver(&self) -> screensaver::ScreenSaverProxy<'_> {
        screensaver::ScreenSaverProxy::new(&self.session)
            .await
            .expect("could not create screen saver proxy")
    }
    pub async fn service_notifications(&self) -> notification::NotificationsProxy<'_> {
        notification::NotificationsProxy::new(&self.session)
            .await
            .expect("could not create notifications proxy")
    }
    pub async fn service_logind_manager(&self) -> logind::Login1ManagerProxy<'_> {
        logind::Login1ManagerProxy::new(&self.system)
            .await
            .expect("could not create logind manager proxy")
    }
    pub async fn service_logind_session(
        &self,
        path: zvariant::OwnedObjectPath,
    ) -> logind::Login1SessionProxy<'_> {
        logind::Login1SessionProxy::builder(&self.system)
            .path(path)
            .expect("Not a valid DBUS path")
            .build()
            .await
            .expect("could not create logind session proxy")
    }
}
