/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use std::path::PathBuf;

pub mod logfile_writer;
pub mod logfile_reader;


fn path() -> PathBuf {
    let xdg = xdg::BaseDirectories::with_prefix("worktime")
        .expect("Failed to load XDG Base Directories for `worktime`");
    xdg.place_data_file("timelog.txt")
        .expect("could not open work log for today")
}
