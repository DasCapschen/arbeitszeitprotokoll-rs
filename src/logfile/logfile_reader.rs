/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use std::fs::File;
use std::io::{BufRead, BufReader};
use time::OffsetDateTime;
use crate::logfile;
use crate::worktime::{WorkTimeEntry, WorkTimeStorage};

pub async fn run(storage: WorkTimeStorage) {
    parse_file(storage);

    // TODO: watch file for changes
    //       if change occured -> parse_file() -> replaces WorkTimeStorage with new values
    //       we parse the whole file again, because external tools might have modified it
}


fn file() -> Result<File, std::io::Error> {
    std::fs::OpenOptions::new()
        .read(true)
        .open(logfile::path())
}

fn parse_file(storage: WorkTimeStorage) {
    let today = OffsetDateTime::now_utc().date();

    let file = file().expect("failed to open file for reading");
    let reader = BufReader::new(file);

    let mut today_entries = Vec::new();

    let mut found_today = false;
    for line in reader.lines().rev() {
        let line = line.expect("failed to read line from file!");
        let entry = WorkTimeEntry::try_from(line.as_str());

        // line could not be parsed, skip it!
        if entry.is_err() {
            continue
        }

        // if we did not reach today yet, skip until we arrive at today
        // if we already parsed today, but now arrive at a different date, stop
        if entry.time.date() != today {
            if found_today {
                break
            } else {
                continue
            }
        }

        found_today = true;
        today_entries.push(entry.unwrap());
    }

    today_entries.reverse();

    let guard = storage.write().unwrap();
    *guard = today_entries;
}

