/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use std::fs::File;
use std::io::{BufRead, BufReader, Write};

use async_std::channel::Receiver;
use time::OffsetDateTime;

use crate::logfile;
use crate::worktime::{WorkTimeEntry, WorkTimeKind};

fn file() -> Result<File, std::io::Error> {
    std::fs::OpenOptions::new()
        .append(true)
        .create(true)
        .write(true)
        .open(logfile::path())
}


pub async fn run(lock_rx: Receiver<OffsetDateTime>, notification_rx: Receiver<(OffsetDateTime, WorkTimeKind)>) {
    // initially log that worktime-rs was started -> assume that work started
    write_to_file(WorkTimeEntry {
        category: Some("automatic".to_string()),
        description: "worktime-rs started".to_string(),
        tags: vec!["worktime-rs".to_string()],
        time: OffsetDateTime::now_utc(),
        kind: WorkTimeKind::Work,
    });

    // TODO: also watch for notification messages
    // if recv() does not return Ok(...), the channel is closed -> stop running
    while let Ok(msg) = lock_rx.recv().await {
        write_to_file(WorkTimeEntry {
            category: Some("automatic".to_string()),
            description: "screen locked".to_string(),
            tags: vec!["worktime-rs".to_string()],
            time: msg,
            kind: WorkTimeKind::Work,
        });
    }

    // log that program was closed -> assume work ended
    write_to_file(WorkTimeEntry {
        category: Some("automatic".to_string()),
        description: "worktime-rs stopped".to_string(),
        tags: vec!["worktime-rs".to_string()],
        time: OffsetDateTime::now_utc(),
        kind: WorkTimeKind::Work,
    });
}

fn write_to_file(entry: WorkTimeEntry) {
    let mut buffer = Vec::new();
    write!(&mut buffer, "{}\n", entry).expect("failed formatting WorkTimeEntry");

    let mut file = Self::file().expect("failed to open file for writing");
    file.write_all(&buffer).expect("failed writing to file");
    file.flush().expect("failed to flush file");
}

   }
}
