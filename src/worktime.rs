/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use std::fmt::{Display, Formatter};
use std::sync::{Arc, RwLock};
use regex::Regex;
use time::{Duration, format_description, OffsetDateTime, PrimitiveDateTime};
use time::macros::datetime;

pub type WorkTimeStorage = Arc<RwLock<Vec<WorkTimeEntry>>>;

// I dislike the (a|b) group at the end, it's kind of redundant, but I cannot get the regex to work otherwise :/
// I feel like (.*?)(--(.*))? should work, but it just matches an empty string...
// I guess .*? is so lazy it just does nothing! .+? only matches 1 character too...
// language=regexp
const LINE_REGEX: &str = r"^([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}): *(?:(.*): *)?(?:(.*?) *-- *(.*)|(.*))";
const TIME_FORMAT: &str = "[year]-[month]-[day] [hour]:[minute]";

pub fn get_total_time(storage: &WorkTimeStorage, kind: WorkTimeKind) -> Duration {
    storage
        .read()
        .expect("couldn't lock storage for reading")
        .iter()
        .filter(|item| item.kind == kind)
        .map(|item| item.duration())
        .fold(Duration::ZERO, |a, b| a + b)
}

#[derive(Eq, PartialEq, Debug)]
pub enum WorkTimeKind {
    Work,    // is working at work
    Pause,   // is taking a break at work
}

impl From<&str> for WorkTimeKind {
    fn from(s: &str) -> Self {
        if s.contains("**") {
            WorkTimeKind::Pause
        } else {
            WorkTimeKind::Work
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
pub struct WorkTimeEntry {
    pub category: Option<String>,
    pub description: String,
    pub tags: Vec<String>,
    pub time: OffsetDateTime,
    pub kind: WorkTimeKind,
}

impl TryFrom<&str> for WorkTimeEntry {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        // format is like
        // yyyy-mm-dd HH:MM: category: description -- tags
        // if there is '**' or '***' anywhere after the date / time, it changes the WorkTimeKind
        // eg: `2022-02-01 12:54: ** project: fix bug -- coding`

        // find out if "***" or "**" are in the string
        // then strip them from the string, we don't want to display them
        let kind = WorkTimeKind::from(value);
        let s = value.replace("***", "").replace("**", "");

        let pattern = Regex::new(LINE_REGEX).map_err(|e| ())?;
        let caps = pattern.captures_iter(&s).next().ok_or(())?;

        let date_time = caps.get(1).unwrap().as_str();
        let category = caps.get(2).map(|m| m.as_str().to_string());
        let description = caps.get(3).map(|m| m.as_str().to_string());
        let tags = caps.get(4).map(|m| m.as_str().to_string());
        let description2 = caps.get(5).map(|m| m.as_str().to_string());

        let format = format_description::parse(TIME_FORMAT).map_err(|e| ())?;
        let timestamp = PrimitiveDateTime::parse(date_time, &format).map_err(|e| ())?.assume_utc();

        Ok(WorkTimeEntry {
            category: category,
            description: description.or(description2).ok_or(())?.trim().to_string(),
            tags: tags.map_or(Vec::new(), |s| s.split_whitespace().map(|s| s.to_string()).collect()),
            time: timestamp,
            kind: kind,
        })
    }
}


impl Display for WorkTimeEntry {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let format = format_description::parse(TIME_FORMAT).unwrap();

        write!(f, "{}: ", self.time.format(&format).unwrap())?;

        if let Some(category) = &self.category {
            write!(f, "{}: ", category)?;
        }

        write!(f, "{}", self.description)?;

        if self.tags.len() > 0 {
            write!(f, " -- ")?;
            for tag in &self.tags {
                write!(f, "{} ", tag)?;
            }
        }

        if self.kind == WorkTimeKind::Pause {
            write!(f, "**")?;
        }

        Ok(())
    }
}

impl WorkTimeEntry {
    pub fn duration(&self) -> Duration {
        todo!()
    }
}


#[test]
fn test_regex() {
    let s1 = "2022-12-01 14:32: description";
    let s2 = "2022-12-01 14:32: project: description";
    let s3 = "2022-12-01 14:32: project: description -- tag1 tag2";
    let s4 = "2022-12-01 14:32: test ***";

    let actual = WorkTimeEntry::from(s1);
    let expected = WorkTimeEntry {
        category: None,
        description: "description".to_string(),
        tags: vec![],
        time: datetime!(2022-12-01 14:32 UTC),
        kind: WorkTimeKind::Work,
    };
    assert_eq!(actual, expected);

    let actual = WorkTimeEntry::from(s2);
    let expected = WorkTimeEntry {
        category: Some("project".to_string()),
        description: "description".to_string(),
        tags: vec![],
        time: datetime!(2022-12-01 14:32 UTC),
        kind: WorkTimeKind::Work,
    };
    assert_eq!(actual, expected);

    let actual = WorkTimeEntry::from(s3);
    let expected = WorkTimeEntry {
        category: Some("project".to_string()),
        description: "description".to_string(),
        tags: vec!["tag1".to_string(), "tag2".to_string()],
        time: datetime!(2022-12-01 14:32 UTC),
        kind: WorkTimeKind::Work,
    };
    assert_eq!(actual, expected);

    let actual = WorkTimeEntry::from(s4);
    let expected = WorkTimeEntry {
        category: None,
        description: "test".to_string(),
        tags: vec![],
        time: datetime!(2022-12-01 14:32 UTC),
        kind: WorkTimeKind::Pause,
    };
    assert_eq!(actual, expected);
}