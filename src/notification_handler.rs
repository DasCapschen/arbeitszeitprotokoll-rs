/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use std::collections::HashMap;

use async_std::channel::{Receiver, Sender};
use async_std::task;
use time::OffsetDateTime;

use crate::Dbus;
use crate::worktime::WorkTimeKind;

pub async fn run(unlock_rx: Receiver<OffsetDateTime>, notification_tx: Sender<(OffsetDateTime, WorkTimeKind)>) {
    let proxy = dbus.service_notifications().await;

    while let Ok(msg) = unlock_rx.recv().await {
        let notification_id = proxy
            .notify(
                "Arbeitszeitprotokoll",
                0,
                "dialog-question",
                "Willkommen zurück!",
                "Hast du gerade eine Pause gemacht?",
                &["pause", "Ja", "work", "Nein"],
                HashMap::new(),
                0, // never expire
            )
            .await
            .expect("Failed sending notification");

        let tx_clone = notification_tx.clone();
        task::spawn(handle_notification(notification_id, move |action| {
            let kind = if action == "work" {
                WorkTimeKind::Work
            } else {
                WorkTimeKind::Pause
            };

            task::block_on(tx_clone.send((msg, kind))).expect("failed to block on writer task");
        }));
    }
}

pub async fn handle_notification<F>(id: u32, func: F)
where
    F: Fn(&str),
{
    let dbus = Dbus::new().await;
    let proxy = dbus.service_notifications().await;
    let mut invoked = proxy.receive_action_invoked().await.unwrap();
    let mut closed = proxy.receive_notification_closed().await.unwrap();

    loop {
        futures::select! {
            signal = invoked.next() => {
                let action = signal.unwrap();
                let args = action.args().unwrap();
                if args.id == id {
                    func(args.action);
                    break
                }
            },
            signal = closed.next() => {
                let action = signal.unwrap();
                let args = action.args().unwrap();
                if args.id == id {
                    break
                }
            }
        }
    }
}
