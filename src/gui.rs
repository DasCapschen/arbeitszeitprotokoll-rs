/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use crate::worktime::WorkTimeKind;
use crate::WorkTimeStorage;
use gtk::glib::Continue;
use gtk::prelude::{ApplicationExt, ContainerExt, LabelExt, PanedExt, WidgetExt, WidgetExtManual};
use gtk::traits::GtkMenuItemExt;
use gtk::{glib, Application, ApplicationWindow, Label, Menu, MenuItem, Orientation, Paned};
use time::Duration;
use libayatana_appindicator::{AppIndicator, AppIndicatorStatus};

pub(crate) fn get_application(storage: WorkTimeStorage) -> Application {
    let app = Application::builder()
        .application_id("de.waurenschk.worktime")
        .build();

    app.connect_activate(move |app| {
        let window = build_gui(app, storage.clone());

        let mut tray_icon = AppIndicator::new("Worktime", "alarm-symbolic");
        tray_icon.set_status(AppIndicatorStatus::Active); // show up (passive = hide)

        let mut tray_menu = Menu::builder().name("Worktime").build();
        let show = MenuItem::builder().label("Show").build();
        let quit = MenuItem::builder().label("Quit").build();
        tray_menu.add(&show);
        tray_menu.add(&quit);
        tray_menu.show_all(); // this is when the tray icon will show up

        tray_icon.set_menu(&mut tray_menu);

        let app_clone = app.clone(); // is just a pointer internally, I hope
        quit.connect_activate(move |_| app_clone.quit());
        let window_clone = window.clone();
        show.connect_activate(move |_| window_clone.show_all());
    });

    app
}

fn build_gui(app: &Application, storage: WorkTimeStorage) -> ApplicationWindow {
    let win = ApplicationWindow::builder()
        .application(app)
        .default_height(200)
        .default_width(360)
        .resizable(false)
        .title("Arbeitszeitprotokoll")
        .build();

    let vbox = gtk::Box::builder()
        .orientation(Orientation::Vertical)
        .build();
    let label_work = Label::builder()
        .label("Arbeitszeit heute: 00:00:00")
        .margin(8)
        .expand(true)
        .use_markup(true)
        .build();
    let label_pause = Label::builder()
        .label("Pausenzeit heute: 00:00:00")
        .margin(8)
        .expand(true)
        .use_markup(true)
        .build();
    vbox.add(&label_work);
    vbox.add(&label_pause);

    let vpane = Paned::builder().orientation(Orientation::Vertical).build();
    vpane.add1(&vbox);
    //vpane.add2(&label_pause);

    win.add(&vpane);

    glib::timeout_add_seconds_local(1, move || {
        let (work_time, pause_time) = {
            let work_time = crate::worktime::get_total_time(&storage, WorkTimeKind::Work);
            let pause_time = crate::worktime::get_total_time(&storage, WorkTimeKind::Pause);
            (work_time, pause_time)
        };

        if work_time.whole_hours() >= 8 {
            label_work.set_markup(&format!(
                "<span size='x-large' bgcolor='#E53935'>Arbeitszeit heute: {}</span>",
                duration_to_hms(&work_time)
            ));
        } else {
            label_work.set_markup(&format!(
                "<span size='x-large'>Arbeitszeit heute: {}</span>",
                duration_to_hms(&work_time)
            ));
        }

        label_pause.set_markup(&format!(
            "<span size='x-large'>Pausenzeit heute: {}</span>",
            duration_to_hms(&pause_time)
        ));

        Continue(true)
    });

    // hide window when closed, but do not close application!
    win.connect_delete_event(|win, _| win.hide_on_delete());
    win
}

fn duration_to_hms(duration: &Duration) -> String {
    format!(
        "{:02}:{:02}:{:02}",
        duration.whole_hours() % 24,
        duration.whole_minutes() % 60,
        duration.whole_seconds() % 60
    )
}
