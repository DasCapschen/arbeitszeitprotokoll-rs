/*
 * Copyright (c) 2022 Dominik Waurenschk.
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

use async_std::channel::{Receiver, Sender};
use futures::StreamExt;
use futures_util::future::Either;
use time::OffsetDateTime;

use crate::Dbus;

pub async fn run(unlock_tx: Sender<OffsetDateTime>, lock_tx: Sender<OffsetDateTime>, sigint_rx: Receiver<()>) {
    let dbus = Dbus::new().await;

    // find the correct session for this user
    // we can use org.freedesktop.login1.Session interface on /org/freedesktop/login1/session/auto
    // for setting and getting values, but ONLY the REAL object has signals!!!
    // calling org.freedesktop.login1.Manager.GetSession("auto") will return the real session,
    // for example /org/freedesktop/login1/session/_32
    let logind_manager = dbus.service_logind_manager().await;
    let session_obj = logind_manager.get_session("auto").await.unwrap();

    // now start the org.freedesktop.login1.Session interface on the correct object
    let logind_session = dbus.service_logind_session(session_obj).await;
    let mut locked_hint_changed = logind_session.receive_locked_hint_changed().await;

    // need to query inital value (wtf, why?!)
    let _ = locked_hint_changed.next().await;


    // I wish I could use the select! macro, it makes the code easier
    // but I really don't understand how it's supposed to work.
    // fuse()ing the stream just makes it so it only ever returns 1 value and then becomes useless
    loop {
        let either = futures::future::select(locked_hint_changed.next(), sigint_rx.recv()).await;
        let signal = match either {
            Either::Left((Some(locked), _)) => locked,
            Either::Right((_, _)) => break,
            _ => continue,
        };

        // get return value from the signal Result<bool, Error>
        let locked_hint = signal.get().await.unwrap();
        if locked_hint {
            lock_tx.send(OffsetDateTime::now_utc()).await.expect("failed sending locked message");
        } else {
            unlock_tx.send(OffsetDateTime::now_utc()).await.expect("failed sending unlocked message");
        }
    }

    // closing the writers signals the receivers that the channel is closed
    // this is their signal to stop (they don't need the sigint channel)
    unlock_tx.close();
    lock_tx.close();
}
