# Arbeitszeitprotokoll-rs

A work-time logger written in Rust.  
Will create logfiles in `~/.local/share/worktime/work_<date>.log` which contain a daily log.  
The logfiles log when you start to work, when you pause (lock your screen) and when you stop working (end the program).  

When you unlock your screen, you will get a notification asking you if you just took a break.  
Select "no" if you locked your screen, but had a meeting instead of a break, for example.  

The app will open as a tray-indicator icon. You can click on the tray icon and select "Show" or "Quit".  
Show will open a window displaying how long you have worked and paused today.

Runtime Dependencies: `gtk3 libayatana-appindicator3`  
DBus is also required (but installed by default anyways)

## Building
Ubuntu dependencies: `build-essential libgtk-3-dev libayatana-appindicator3-dev`  
Build: `cargo build`  
Install: `cargo install --path .` (will install to `~/.cargo/bin/arbeitszeitprotokoll-rs`)
